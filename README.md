# bbedit-scripts

These are scripts to extend the functionality of
[BBEdit](https://www.barebones.com/products/bbedit/).

## Supported Tools

The following are the tools currently supported by these scripts.
You need to have the specific tool installed for a given script to work.

### Git

The [Git](https://git-scm.com/) Source Code Manager.

Specifically, the `git init` command.

### Markdown Tools

The [Markdown](https://en.wikipedia.org/wiki/Markdown) lightweight markup language.

* [mdl](https://github.com/markdownlint/markdownlint) (MarkdownLint):
  Checks formatting and style issues (linting).

### Ruby Tools

The [Ruby](https://www.ruby-lang.org/) scripting language and the
[Ruby on Rails](https://rubyonrails.org/) web application framework:

* [Brakeman](http://brakemanscanner.org/):
  Security analysis for Ruby on Rails projects.
* [Rails Best Practices](https://github.com/railsbp/rails_best_practices):
  Checking for following of “best practices” for Ruby on Rails projects.
* [Reek](https://github.com/troessner/reek):
  Checks Ruby source files for “code smells”.
* [RSpec](https://rspec.info/):
  Unit/integration/feature testing for Ruby code.
* [RuboCop](https://github.com/rubocop-hq/rubocop/):
  Static code analysis (linting/linter) for Ruby source files.

### Shell Tools

Tools to help with writing shell scripts (sh, bash, zsh, etc.).

* [ShellCheck](https://github.com/koalaman/shellcheck):
  Code validator/formatter/linter for shell scripts.

### Swift Tools

* [SwiftLint](https://github.com/realm/SwiftLint):
  Static code analysis (linter) for Swift source files.

### YAML Tools

* [yamllint](https://github.com/adrienverge/yamllint):
  Linting/validation of YAML (.yml) files.

## Installation

Copy the scripts you want to use to your
`~/Library/Application Support/BBEdit/Scripts` directory.

You can change the names of the scripts to your liking.
Note that you can override alphabetical sorting by prepending a 2-digit number
followed by a close-bracket (“)”) to the filename.

E.g., `01)Z Script.sh` will appear as “Z Script”, and come before `A Script.sh`,
in the Scripts menu.

### The RSpec script is special

The `RSpec.sh` script relies on the `.bbedit_formatter.rb` ruby script which
must be in the same directory as it (normally the BBEdit Scripts directory).

If you are doing a Finder copy, you may not be able to see the file
(files with a leading `.` are hidden in the Finder).

To copy it, use the Terminal app (found in the `/Applications/Utilities`
directory) to navigate to the source directory for the scripts,
then use `cp` to copy the hidden file:

    cp ".bbedit_formatter.rb" ~/Library/Applicaton\ Support/BBEdit/Scripts

(You may need to modify the destination in that command if you are putting
the RSpec.sh script somewhere other than the root Scripts folder for BBEdit.)

## Usage

Once installed, the Scripts will be available under the “Scripts” menu in BBEdit.

The results of processing a given script will go into a BBEdit results window,
which will list the results, making each clickable to go directly to the
applicable source file and line.

For example, an RSpec test error will show as an entry in the results window,
with the explanation of the error, which you can then click on to go directly
to the failing test in your source.

## License

This project is free to use as open source under the [MIT License](LICENSE).

## Contributors

* [Grant Neufeld](http://grantneufeld.ca/).
  Twitter: [@grant](https://twitter.com/grant)
