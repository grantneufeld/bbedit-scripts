#!/bin/bash
#
# Rails Best Practices script for the BBEdit Scripts menu.
# https://gitlab.com/grantneufeld/bbedit-scripts
#
# DEPENDENCIES:
# - [Rails Best Practices](https://github.com/railsbp/rails_best_practices) gem.
#
# OPTIONALLY SUPPORTED (if present):
# - [Bundler](https://github.com/rubygems/bundler) gem.
# - [RVM](https://rvm.io/).

# Test for RVM installed:
if ! which rvm > /dev/null 2> /dev/null
then
  # ignore it
  :
else
  # Load RVM into a shell session *as a function*
  [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
fi

STARTDIR=$(pwd)

# Figure out the project root directory, if there is one.
# Else, the directory of the current file.
if [ -n "$BBEDIT_INSTAPROJECT_ROOT" ]; then
  DIR="$BBEDIT_INSTAPROJECT_ROOT"
elif [ -n "$BBEDIT_ACTIVE_PROJECT" ]; then
  DIR=$(dirname "$BBEDIT_ACTIVE_PROJECT")
else
  DIR=""
fi

if [ -n "$DIR" ]; then
  cd "$DIR"

  # Test for Rails Best Practices installed:
  if ! which rails_best_practices > /dev/null 2> /dev/null
  then
    echo "Rails Best Practices is not installed."
    exit 1
  fi

  # make sure we’re in a rails project directory
  if [ -e "config.ru" ]; then
    # determine whether to “bundle exec” or not
    if [ -e ./GEMFILE.lock ]; then
      bundle exec rails_best_practices --silent --format text --spec --features --without-color | bbresults --pattern "(?P<file>.+?):(?P<line>\d+)\s+-? *(?P<msg>.*)$"
    else
      rails_best_practices --silent --format text --spec --features --without-color | bbresults --pattern "(?P<file>.+?):(?P<line>\d+)\s+-? *(?P<msg>.*)$"
    fi
  else
    echo "Apparently not a Ruby on Rails project."
  fi
else
  echo "Not a valid project."
fi

cd "$STARTDIR"
