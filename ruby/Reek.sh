#!/bin/bash
#
# Reek script for the BBEdit Scripts menu.
# https://gitlab.com/grantneufeld/bbedit-scripts
#
# DEPENDENCIES:
# - [Reek](https://github.com/troessner/reek) gem.
#
# OPTIONALLY SUPPORTED (if present):
# - [Bundler](https://github.com/rubygems/bundler) gem.
# - [RVM](https://rvm.io/).

# Test for RVM installed:
if ! which rvm > /dev/null 2> /dev/null
then
  # ignore it
  :
else
  # Load RVM into a shell session *as a function*
  [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
fi

STARTDIR=$(pwd)

TARGET="."
# Figure out the project root directory, if there is one.
# Else, the directory of the current file.
if [ -n "$BBEDIT_INSTAPROJECT_ROOT" ]; then
  DIR="$BBEDIT_INSTAPROJECT_ROOT"
elif [ -n "$BBEDIT_ACTIVE_PROJECT" ]; then
  DIR=$(dirname "$BBEDIT_ACTIVE_PROJECT")
else
  DIR=$(dirname "$BB_DOC_PATH")
  TARGET="$BB_DOC_PATH"
fi

cd "$DIR"

# Test for Reek installed:
if ! which reek > /dev/null 2> /dev/null
then
  echo "Reek is not installed."
  exit 1
fi

# Run Reek
if [ -e ./GEMFILE.lock ]; then
  TARGET="$TARGET" bundle exec reek --no-color --single-line --no-progress --format text "$TARGET" | bbresults --pattern "^ *(?P<file>[^:\r\n]+):(?P<line>\d+): *(?P<msg>.*)$"
else
  TARGET="$TARGET" reek --no-color --single-line --no-progress --format text "$TARGET" | bbresults --pattern "^ *(?P<file>[^:\r\n]+):(?P<line>\d+): *(?P<msg>.*)$"
fi

cd "$STARTDIR"
