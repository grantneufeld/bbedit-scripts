#!/bin/bash
#
# RSpec script for the BBEdit Scripts menu.
# https://gitlab.com/grantneufeld/bbedit-scripts
#
# DEPENDENCIES:
# - [RSpec](https://rspec.info/) gem.
#
# OPTIONALLY SUPPORTED (if present):
# - [Bundler](https://github.com/rubygems/bundler) gem.
# - [RVM](https://rvm.io/).

# Test for RVM installed:
if ! which rvm > /dev/null 2> /dev/null
then
  # ignore it
  :
else
  # Load RVM into a shell session *as a function*
  [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
fi

STARTDIR=$(pwd)

TARGET="."
# Figure out the project root directory, if there is one.
# Else, the directory of the current file.
if [ -n "$BBEDIT_INSTAPROJECT_ROOT" ]; then
  DIR="$BBEDIT_INSTAPROJECT_ROOT"
elif [ -n "$BBEDIT_ACTIVE_PROJECT" ]; then
  DIR=$(dirname "$BBEDIT_ACTIVE_PROJECT")
else
  DIR=$(dirname "$BB_DOC_PATH")
  TARGET="$BB_DOC_PATH"
fi

cd "$DIR"

# Test for RSpec installed:
if ! which rspec > /dev/null 2> /dev/null
then
  echo "RSpec is not installed."
  exit 1
fi

# call rspec with our custom rspec formatter
if [ -e ./GEMFILE.lock ]; then
  TARGET="$TARGET" bundle exec rspec --order defined --no-profile --require "$STARTDIR/.bbedit_formatter.rb" --format BbeditFormatter "$TARGET" 2> /dev/null | bbresults --errors-default
else
  TARGET="$TARGET" rspec --order defined --no-profile --require "$STARTDIR/.bbedit_formatter.rb" --format BbeditFormatter "$TARGET" 2> /dev/null | bbresults --errors-default
fi

cd "$STARTDIR"
