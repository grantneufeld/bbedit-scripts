#!/bin/bash
#
# Brakeman script for the BBEdit Scripts menu.
# https://gitlab.com/grantneufeld/bbedit-scripts
#
# DEPENDENCIES:
# - [Brakeman](http://brakemanscanner.org/) gem.
#
# OPTIONALLY SUPPORTED (if present):
# - [Bundler](https://github.com/rubygems/bundler) gem.
# - [RVM](https://rvm.io/).

# Test for RVM installed:
if ! which rvm > /dev/null 2> /dev/null
then
  # ignore it
  :
else
  # Load RVM into a shell session *as a function*
  [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
fi

STARTDIR=$(pwd)

# Figure out the project root directory, if there is one.
# Else, the directory of the current file.
if [ -n "$BBEDIT_INSTAPROJECT_ROOT" ]; then
  DIR="$BBEDIT_INSTAPROJECT_ROOT"
elif [ -n "$BBEDIT_ACTIVE_PROJECT" ]; then
  DIR=$(dirname "$BBEDIT_ACTIVE_PROJECT")
else
  DIR=""
fi

if [ -n "$DIR" ]; then
  cd "$DIR"

  # Test for Brakeman installed:
  if ! which brakeman > /dev/null 2> /dev/null
  then
    echo "Brakeman is not installed."
    exit 1
  fi

  # make sure we’re in a rails project directory
  if [ -e "config.ru" ]; then
    # determine whether to “bundle exec” or not
    if [ -e ./GEMFILE.lock ]; then
      bundle exec brakeman --quiet --separate-models --confidence-level 1 --no-color --no-pager --format tabs | bbresults --pattern "^(?P<file>[^\t\r\n]+)\t(?P<line>\d+)\t.*\t(?P<msg>[^\t\r\n]*)\t[A-Za-z]+$"
    else
      brakeman --quiet --separate-models --confidence-level 1 --no-color --no-pager --format tabs | bbresults --pattern "^(?P<file>[^\t\r\n]+)\t(?P<line>\d+)\t.*\t(?P<msg>[^\t\r\n]*)\t[A-Za-z]+$"
    fi
  else
    echo "Apparently not a Ruby on Rails project."
  fi
else
  echo "Not a valid project."
fi

cd "$STARTDIR"
