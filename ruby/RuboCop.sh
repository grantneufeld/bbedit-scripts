#!/bin/bash
#
# RuboCop script for the BBEdit Scripts menu.
# https://gitlab.com/grantneufeld/bbedit-scripts
#
# DEPENDENCIES:
# - [RuboCop](https://github.com/rubocop-hq/rubocop/) gem.
#
# OPTIONALLY SUPPORTED (if present):
# - [Bundler](https://github.com/rubygems/bundler) gem.
# - [RVM](https://rvm.io/).

# Test for RVM installed:
if ! which rvm > /dev/null 2> /dev/null
then
  # ignore it
  :
else
  # Load RVM into a shell session *as a function*
  [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
fi

STARTDIR=$(pwd)

TARGET="."
# Figure out the project root directory, if there is one.
# Else, the directory of the current file.
if [ -n "$BBEDIT_INSTAPROJECT_ROOT" ]; then
  DIR="$BBEDIT_INSTAPROJECT_ROOT"
elif [ -n "$BBEDIT_ACTIVE_PROJECT" ]; then
  DIR=$(dirname "$BBEDIT_ACTIVE_PROJECT")
else
  DIR=$(dirname "$BB_DOC_PATH")
  TARGET="$BB_DOC_PATH"
fi

cd "$DIR"

# Test for RuboCop installed:
if ! which rubocop > /dev/null 2> /dev/null
then
  echo "RuboCop is not installed."
  exit 1
fi

# Run Rubocop
if [ -e "config/rubocop.yml" ]; then
  # run with a custom config file path
  if [ -e ./GEMFILE.lock ]; then
    TARGET="$TARGET" bundle exec rubocop --config config/rubocop.yml --format e "$TARGET" | bbresults
  else
    TARGET="$TARGET" rubocop --config config/rubocop.yml --format e "$TARGET" | bbresults
  fi
else
  # use rubocop’s normal config lookup
  if [ -e ./GEMFILE.lock ]; then
    TARGET="$TARGET" bundle exec rubocop --format e "$TARGET" | bbresults
  else
    TARGET="$TARGET" rubocop --format e "$TARGET" | bbresults
  fi
fi

cd "$STARTDIR"
