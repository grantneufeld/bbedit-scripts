# Ref: https://ieftimov.com/post/how-to-write-rspec-formatters-from-scratch/

# Custom RSpec formatter, used by the RSpec.sh script.
class BbeditFormatter
  RSpec::Core::Formatters.register self, :dump_failures, :close

  attr_reader :output

  def initialize(output)
    @output = output
  end

  # Output each failure as a line of text:
  # filepath:line_number: exception_message
  def dump_failures(notification)
    @output << notification.failed_examples.map do |example|
      "#{example.location}: #{example.execution_result.exception.message}\n"
    end.join('')
  end

  def close(_notification)
    @output << '\n'
  end
end
