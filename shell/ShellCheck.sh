#!/bin/sh
#
# ShellCheck script for the BBEdit Scripts menu.
# https://gitlab.com/grantneufeld/bbedit-scripts
#
# DEPENDENCIES:
# - [ShellCheck](https://github.com/koalaman/shellcheck).
# - ZShell (zsh) (to work around filepath globbing issue).

# Test for shellcheck installed:
if ! which shellcheck > /dev/null 2> /dev/null
then
  echo "shellcheck is not installed."
  exit 1
fi

STARTDIR=$(pwd)

TARGET=""
# Figure out the project root directory, if there is one.
# Else, the directory of the current file.
if [ -n "$BBEDIT_INSTAPROJECT_ROOT" ]; then
  DIR="$BBEDIT_INSTAPROJECT_ROOT"
elif [ -n "$BBEDIT_ACTIVE_PROJECT" ]; then
  DIR=$(dirname "$BBEDIT_ACTIVE_PROJECT")
else
  DIR=$(dirname "$BB_DOC_PATH")
  TARGET="$BB_DOC_PATH"
fi

cd "$DIR"

# Run shellcheck
if [ -n $TARGET ]; then
  # project directory (and sub-directories)
  zsh -c 'shellcheck --check-sourced --color=never --format=gcc --enable=add-default-case,avoid-nullary-conditions,check-unassigned-uppercase,quote-safe-variables,require-variable-braces ./**/*.sh' | bbresults
  # the "./**/*.sh" specifies to look for ".sh" files in
  # the current directory and any sub-directories
else
  # an individual shell script file
  TARGET="$TARGET" shellcheck --check-sourced --color=never --format=gcc --enable=add-default-case,avoid-nullary-conditions,check-unassigned-uppercase,quote-safe-variables,require-variable-braces "$TARGET" | bbresults
fi

cd "$STARTDIR"
