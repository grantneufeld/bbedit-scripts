#!/bin/bash
#
# Markdown Linter (MDL) script for the BBEdit Scripts menu.
# https://gitlab.com/grantneufeld/bbedit-scripts
#
# You can configure the rule “styles” in `config/markdown_style.rb`
# in the root of your project,
# or relative to the individual file you are checking (if not a project).
#
# DEPENDENCIES:
# - [mdl](https://github.com/markdownlint/markdownlint) gem.
#
# OPTIONALLY SUPPORTED (if present):
# - [Bundler](https://github.com/rubygems/bundler) gem.
# - [RVM](https://rvm.io/).

# Test for RVM installed:
if ! which rvm > /dev/null 2> /dev/null
then
  # ignore it
  :
else
  # Load RVM into a shell session *as a function*
  [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
fi

STARTDIR=$(pwd)

NODIR=""
# Figure out the project root directory, if there is one.
# Else, the directory of the current file.
if [ -n "$BBEDIT_INSTAPROJECT_ROOT" ]; then
  DIR="$BBEDIT_INSTAPROJECT_ROOT"
elif [ -n "$BBEDIT_ACTIVE_PROJECT" ]; then
  DIR=$(dirname "$BBEDIT_ACTIVE_PROJECT")
else
  DIR=$(dirname "$BB_DOC_PATH")
  NODIR="$BB_DOC_PATH"
fi

if [ -n "$NODIR" ]; then
  TARGET="$NODIR"
else
  TARGET="$DIR"
fi

cd "$DIR"

# Test for mdl installed:
if ! which mdl > /dev/null 2> /dev/null
then
  echo "mdl is not installed."
  exit 1
fi

# Run mdl
if [ -e ./config/markdown_style.rb ]; then
  if [ -e ./GEMFILE.lock ]; then
    TARGET="$TARGET" bundle exec mdl --warnings --style config/markdown_style.rb "$TARGET" | bbresults
  else
    TARGET="$TARGET" mdl --warnings --style config/markdown_style.rb "$TARGET" | bbresults
  fi
else
  if [ -e ./GEMFILE.lock ]; then
    TARGET="$TARGET" bundle exec mdl --warnings "$TARGET" | bbresults
  else
    TARGET="$TARGET" mdl --warnings "$TARGET" | bbresults
  fi
fi

cd "$STARTDIR"
