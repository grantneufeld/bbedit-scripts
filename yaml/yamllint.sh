#!/bin/sh
#
# yamllint script for the BBEdit Scripts menu.
# https://gitlab.com/grantneufeld/bbedit-scripts
#
# DEPENDENCIES:
# - [yamllint](https://github.com/adrienverge/yamllint).

# Test for yamllint installed:
if ! which yamllint > /dev/null 2> /dev/null
then
  echo "yamllint is not installed."
  exit 1
fi

STARTDIR=$(pwd)

TARGET="."
# Figure out the project root directory, if there is one.
# Else, the directory of the current file.
if [ -n "$BBEDIT_INSTAPROJECT_ROOT" ]; then
  DIR="$BBEDIT_INSTAPROJECT_ROOT"
elif [ -n "$BBEDIT_ACTIVE_PROJECT" ]; then
  DIR=$(dirname "$BBEDIT_ACTIVE_PROJECT")
else
  DIR=$(dirname "$BB_DOC_PATH")
  TARGET="$BB_DOC_PATH"
fi

cd "$DIR"

if [ -e "config/yamllint.yml" ]; then
  # run with a custom config file path
  TARGET="$TARGET" yamllint -c config/yamllint.yml --format parsable "$TARGET" | bbresults
else
  # use yamllint’s default config lookup
  TARGET="$TARGET" yamllint --format parsable "$TARGET" | bbresults
fi

cd "$STARTDIR"
