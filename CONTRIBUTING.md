# Contributing to these BBEdit Scripts

[Issue submissions](https://gitlab.com/grantneufeld/bbedit-scripts/issues)
and merge/pull requests welcome.

(Please prefer to use the Issues board here rather than contacting the
developer(s) through other means.
I’d like to keep this project as open and transparent as possible.)

By submitting code or any other content to this project,
you assert that you have the right to contribute such content,
and accept that all contributions will go under the
[MIT License](LICENSE) with the rest of this project.

**Thank-you!**
