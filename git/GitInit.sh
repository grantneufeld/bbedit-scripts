#!/bin/bash
#
# Initialize a Git repository for the current project directory.
# https://gitlab.com/grantneufeld/bbedit-scripts
#
# DEPENDENCIES:
# - [Git](https://git-scm.com/).
#
# OPTIONALLY SUPPORTED (if present):
# - GitX (an old macOS GUI app for Git).

# Test for RSpec installed:
if ! which git > /dev/null 2> /dev/null
then
  echo "Git is not installed."
  exit 1
fi

STARTDIR=$(pwd)

TARGET="."
# Figure out the project root directory, if there is one.
# Else, the directory of the current file.
if [ -n "$BBEDIT_INSTAPROJECT_ROOT" ]; then
  DIR="$BBEDIT_INSTAPROJECT_ROOT"
elif [ -n "$BBEDIT_ACTIVE_PROJECT" ]; then
  DIR=$(dirname "$BBEDIT_ACTIVE_PROJECT")
else
  DIR=$(dirname "$BB_DOC_PATH")
  TARGET="$BB_DOC_PATH"
fi

cd "$DIR"

# check if already initialized
if [ -e ./.git ]; then
  echo "This project directory already has git initialized."
  cd "$STARTDIR"
  exit 2
fi

# Everything looks right to be able to initialize Git here:
git init

# load GitX if it’s installed
if which gitx > /dev/null 2> /dev/null
then
  gitx
fi

cd "$STARTDIR"
