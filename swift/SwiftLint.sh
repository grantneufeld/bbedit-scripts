#!/bin/sh
#
# SwiftLint script for the BBEdit Scripts menu.
# https://gitlab.com/grantneufeld/bbedit-scripts
#
# DEPENDENCIES:
# - [SwiftLint](https://github.com/realm/SwiftLint).

# Test for swiftlint installed:
if ! which swiftlint > /dev/null 2> /dev/null
then
  echo "swiftlint is not installed."
  exit 1
fi

STARTDIR=$(pwd)

TARGET="."
# Figure out the project root directory, if there is one.
# Else, the directory of the current file.
if [ -n "$BBEDIT_INSTAPROJECT_ROOT" ]; then
  DIR="$BBEDIT_INSTAPROJECT_ROOT"
elif [ -n "$BBEDIT_ACTIVE_PROJECT" ]; then
  DIR=$(dirname "$BBEDIT_ACTIVE_PROJECT")
else
  DIR=$(dirname "$BB_DOC_PATH")
  TARGET="$BB_DOC_PATH"
fi

cd "$DIR"

# Run swiftlint
TARGET="$TARGET" swiftlint lint "$TARGET" | bbresults

cd "$STARTDIR"
